package com.example.productviewer.ui.expensive;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.productviewer.ui.productsAdapter;

public class expensiveFragment extends Fragment {

    private String[] list = new String[]{"this" , "is" , "the" , "most" , "expensive"
            , "products" , "fragment" , "just" , "testing" , "the " , "scrolling" ,
            "bar" , "for" , "dummy" , "data"};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recycle = new RecyclerView(getContext());
        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recycle.addItemDecoration(new DividerItemDecoration(getContext()
                , DividerItemDecoration.VERTICAL));
        recycle.setAdapter(new productsAdapter(list));
        return recycle;
    }
}