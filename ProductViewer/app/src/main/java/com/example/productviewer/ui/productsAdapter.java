package com.example.productviewer.ui;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class productsAdapter extends RecyclerView.Adapter<productsAdapter.viewHolder> {
    @NonNull
    private String [] Data;
    public productsAdapter(String[] d)
    {
        Data = d;
    }
    @SuppressLint("ResourceType")
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        TextView temp = new TextView(parent.getContext());
        temp.setTextSize(40);
        View v = temp;
        viewHolder view = new viewHolder((TextView) v);
        return view;
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.textView.setText(Data[position]);
    }

    @Override
    public int getItemCount() {
        return Data.length;
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        public viewHolder(TextView v) {
            super(v);
            textView = v;
        }
    }
}